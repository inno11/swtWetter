export class WetterWebServerPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('wetter-web-server-app h1')).getText();
  }
}
