import { WetterWebServerPage } from './app.po';

describe('wetter-web-server App', function() {
  let page: WetterWebServerPage;

  beforeEach(() => {
    page = new WetterWebServerPage();
  })

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('wetter-web-server works!');
  });
});
