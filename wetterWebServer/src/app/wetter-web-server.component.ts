import { Component } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'wetter-web-server-app',
  templateUrl: 'wetter-web-server.component.html',
  styleUrls: ['wetter-web-server.component.css']
})
export class WetterWebServerAppComponent {
  title = 'wetter-web-server works!';
}
