import {
  beforeEachProviders,
  it,
  describe,
  expect,
  inject
} from '@angular/core/testing';
import { Service1Service } from './service1.service';

describe('Service1 Service', () => {
  beforeEachProviders(() => [Service1Service]);

  it('should ...',
      inject([Service1Service], (service: Service1Service) => {
    expect(service).toBeTruthy();
  }));
});
