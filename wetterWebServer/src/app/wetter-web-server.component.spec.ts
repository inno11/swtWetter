import {
  beforeEachProviders,
  describe,
  expect,
  it,
  inject
} from '@angular/core/testing';
import { WetterWebServerAppComponent } from '../app/wetter-web-server.component';

beforeEachProviders(() => [WetterWebServerAppComponent]);

describe('App: WetterWebServer', () => {
  it('should create the app',
      inject([WetterWebServerAppComponent], (app: WetterWebServerAppComponent) => {
    expect(app).toBeTruthy();
  }));

  it('should have as title \'wetter-web-server works!\'',
      inject([WetterWebServerAppComponent], (app: WetterWebServerAppComponent) => {
    expect(app.title).toEqual('wetter-web-server works!');
  }));
});
